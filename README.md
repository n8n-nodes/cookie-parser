![Banner image](https://user-images.githubusercontent.com/10284570/173569848-c624317f-42b1-45a6-ab09-f0ea3c247648.png)
## CookieParser Node for n8n

The `CookieParser` node is a custom node designed for [n8n](https://n8n.io/) that transforms a string representing cookies into a JSON object.

## Features

- Set the path to retrieve the cookie string (`headers.cookie`).
- Accepts a string formatted as a cookie list (`name=value;`).
- Outputs a JSON object where each cookie name is a key and each cookie value is the corresponding value.

## Installation

Follow the [installation guide](https://docs.n8n.io/integrations/community-nodes/installation/) in the n8n community nodes documentation.

## Usage

1. Add the `CookieParser` node to your n8n workflow.
2. Connect the node to your data source.
3. Configure the node to accept the cookie string as input.
4. Run the workflow, and the node will output a parsed JSON object.

## Example

### Input

```text
name1=value1; name2=value2; name3=value3
```

### Output

```json
{
  "name1": "value1",
  "name2": "value2",
  "name3": "value3"
}
```

## Credentials

no authentication required

## Resources

* [n8n community nodes documentation](https://docs.n8n.io/integrations/community-nodes/)
* _Link to app/service documentation._

## More information

Refer to our [documentation on creating nodes](https://docs.n8n.io/integrations/creating-nodes/) for detailed information on building your own nodes.

## License

[MIT](https://github.com/n8n-io/n8n-nodes-starter/blob/master/LICENSE.md)
