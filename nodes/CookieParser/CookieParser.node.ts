import {
  IExecuteFunctions,
} from 'n8n-core';
import {
  INodeExecutionData,
  INodeType,
  INodeTypeDescription,
} from 'n8n-workflow';

export class CookieParser implements INodeType {
  description: INodeTypeDescription = {
    displayName: 'Cookie Parser',
    name: 'CookieParser',
    group: ['transform'],
    version: 1,
    description: 'Parses a cookie string into an object',
    defaults: {
      name: 'Cookie Parser',
     //color: '#772244',
    },
    inputs: ['main'],
    outputs: ['main'],
    properties: [
      {
        displayName: 'Cookie String',
        name: 'cookieString',
        type: 'string',
        default: 'headers.cookie',
      },
    ],
  };
  
  async execute(this: IExecuteFunctions): Promise<INodeExecutionData[][]> {
    const items = this.getInputData();
    const returnData = [];
	
    const cookieString = this.getNodeParameter('cookieString', 0) as string;
	const cookieArray=cookieString.split('.')
    let cookieValue: any
    let item: INodeExecutionData;
    for (let itemIndex = 0; itemIndex < items.length; itemIndex++) {
      item = items[itemIndex];
	  if('json' in item){
		const data= item['json']
		cookieValue = {...data}
		for(let k of cookieArray){
			if(k in cookieValue)
				cookieValue=cookieValue[k] ||''
		}
	  } 
	  if(typeof cookieValue === 'string'){
		  const cookieObject: { [key: string]: string } = {};
		  const cookiePairs = cookieValue.split(';');
		  for (let pair of cookiePairs) {
			pair = pair.trim();
			const [name, value] = pair.split('=');
			cookieObject[name] = value;
		  }
		  returnData.push({
			json: cookieObject,
		  });
	   }
    }

    return [returnData];
  }
}
